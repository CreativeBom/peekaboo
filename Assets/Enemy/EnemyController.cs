﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public GameObject Enemy;
	public float PosX;
	public float PosZ;
	public float BornRate = 3.0f;
	private float NextBorn = 0.0f;

	public enum Choose{
		Default,Regular
	}

	public Choose Mode;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		switch(Mode){
			case Choose.Default:
				if(Time.timeSinceLevelLoad > NextBorn){
					PosX = Random.Range(-80.0f,70.0f);
					PosZ = Random.Range(-80.0f,77.0f);
					Instantiate(Enemy, new Vector3(PosX, 15.0f, PosZ), Quaternion.identity);
					NextBorn = Time.timeSinceLevelLoad + BornRate;
				}
				break;

			case Choose.Regular:
				if(Time.timeSinceLevelLoad > NextBorn){
					Instantiate(Enemy, this.gameObject.transform.position, Quaternion.identity);
					NextBorn = Time.timeSinceLevelLoad + BornRate;
				}
				break;
		}
	}
}