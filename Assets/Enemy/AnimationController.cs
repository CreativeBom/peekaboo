﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		anim.SetBool("Atk",true);
	}
	
	// Update is called once per frame
	void Update () {
		//AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

	}

	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		anim.SetBool("Atk",true);
	}
	
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Touch"){
			anim.SetBool("Atk",false);
		}
	}

	void OnTriggerStay(Collider other) {
		if(other.gameObject.tag == "Touch"){
			anim.SetBool("Atk",false);
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.gameObject.tag == "Touch"){
			StartCoroutine(WaitAndPrint(3.0F));
		}
	}
}
