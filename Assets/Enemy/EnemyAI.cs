﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	private Transform myTransform;
	public GameObject EnemyTarget;
	public int EnemyMovespeed;
	public int EnemyRotationspeed;
	public bool rotate;
	public TimeController TC; 
	public GameObject ShowStar;
	
	void Awake(){
		myTransform = transform;
	}
	
	// Use this for initialization
	void Start () {
		GameObject TimeObj = GameObject.FindGameObjectWithTag("Time");
		EnemyTarget = GameObject.FindGameObjectWithTag("Player");
		TC = TimeObj.GetComponent<TimeController>();
		rotate = false;
		ShowStar.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

		EnemyBehavior(EnemyTarget.transform, EnemyMovespeed, EnemyRotationspeed);
			
	}

	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		EnemyMovespeed = 3;
		ShowStar.SetActive(false);
	}
	
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			TC.GameOver = true;
		}
		else if(other.gameObject.tag == "Touch"){
			EnemyMovespeed = 0;
			ShowStar.SetActive(true);
		}
	}

	void OnTriggerStay(Collider other) {
		if(other.gameObject.tag == "Touch"){
			EnemyMovespeed = 0;
			ShowStar.SetActive(true);
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.gameObject.tag == "Touch"){
			StartCoroutine(WaitAndPrint(3.0F));
		}
	}

	void EnemyBehavior(Transform target,int movespeed,int rotationspeed){
		Debug.DrawLine(target.position, myTransform.position, Color.yellow);
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;
		float rotation = Random.Range(45.0f,180.0f);
		
		myTransform.position += myTransform.forward * movespeed * Time.deltaTime;
		
		Debug.DrawRay(transform.position,(transform.forward + transform.right) * 3,Color.red);
		Debug.DrawRay(transform.position,(transform.forward - transform.right) * 3,Color.red);
		Debug.DrawRay(transform.position,transform.forward * 5,Color.red);
		
		if(Physics.Raycast(transform.position, transform.forward * 5, out hit , 5.0f)){
			print("There is something in front of the object!");
			if(hit.transform.tag == "cube" || hit.transform.tag == "Untagged"){
				rotate = true;
			}
		}
		if(Physics.Raycast(transform.position, (transform.forward - transform.right) * 3, out hit , 5.0f)){
			print("There is something in front of the object!");
			if(hit.transform.tag == "cube" || hit.transform.tag == "Untagged"){
				rotate = true;
			}
		}
		if(Physics.Raycast(transform.position, (transform.forward + transform.right) * 3, out hit , 5.0f)){
			print("There is something in front of the object!");
			if(hit.transform.tag == "cube" || hit.transform.tag == "Untagged"){
				rotate = true;
			}
		}
		
		if(rotate == true){
			transform.Rotate(0,rotation,0);
			rotate = false;
		}
		
		Vector3 delta = target.position - myTransform.position;
		delta.y = 0;
		delta.Normalize();
		
		myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationspeed * Time.deltaTime);
		myTransform.position += myTransform.forward * movespeed * Time.deltaTime;
	}
}
