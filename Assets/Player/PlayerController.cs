﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float Rotatespeed;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		if(Input.GetKey(KeyCode.A)){
			transform.Rotate(0.0f, -Rotatespeed * Time.deltaTime,0.0f);
		}

		else if(Input.GetKey(KeyCode.D)){
			transform.Rotate(0.0f, Rotatespeed * Time.deltaTime,0.0f);
		}
	}
}
