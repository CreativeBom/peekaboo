﻿using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {
	public GUIStyle TimeStyle;
	public GUIStyle TextStyle;
	public int min,sec;
	private float Sw;
	public bool GameOver = false;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
	}
	
	// Update is called once per frame
	void Update () {
		min = (int)Time.timeSinceLevelLoad/60;
		sec = (int)Time.timeSinceLevelLoad%60;
	}

	void OnGUI(){
		if(GameOver == true){
			Time.timeScale = 0;
			GUI.Label(new Rect(Sw/2-300,130,600,100), "Game Over" ,TextStyle);
			GUI.Label(new Rect(Sw/2-300,250,600,100), min + ":" + sec ,TimeStyle);
			if(GUI.Button(new Rect(Sw/2-50,400,100,100),"Again")){
				Application.LoadLevel("GameScene");
			}
		}
		else{
			GUI.Label(new Rect(Sw/2-50,30,100,100), min + ":" + sec ,TimeStyle);
			Time.timeScale = 1;
		}
	}
}
